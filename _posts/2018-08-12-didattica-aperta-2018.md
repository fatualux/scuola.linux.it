---
categories:
- community
layout: post
title: Didattica Aperta 2018
---
Il 14/15 settembre si terrà, presso l'università di Bologna, l'edizione 2018 di <a href="http://www.didatticaaperta.it/">Didattica Aperta</a>, convegno interamente dedicato all'adozione del software libero e opensource a scuola.

<!--more-->

Tra i talk in programma segnaliamo in particolare quelli che riprendono community e temi già segnalati qui su <a href="http://scuola.linux.it/">Scuola.Linux.it</a>: quello dedicato a <a href="https://sodilinux.itd.cnr.it/">So.Di.Linux</a>, la popolare distribuzione Linux per la scuola, quello sui materiali didattici collaborativamente redatti <a href="http://scuola.linux.it/wikiversity-scuole-elementari-medie">su WIkiversity</a>, e quello sulle <a href="http://scuola.linux.it/voting-machine-lombardia">"voting machine" con Ubuntu</a> distribuite presso le scuole della Lombardia.

Al convegno sono invitati insegnanti e professori delle scuole di ogni grado, nonché genitori sensibili al tema delle libertà digitali.
