---
categories:
- community
layout: post
title: Mappando la Community
---
<a href="http://scuola.linux.it/mappe-scuola-software-libero">Pubblicato qui</a> un elenco delle diverse mappe mantenute dalle diverse community pro-freesoftware per indicizzare e catalogare le adozioni di software libero nelle scuole italiane.

Confidiamo che questo possa essere un utile strumento per chi è alla ricerca di altre persone che già stanno utilizzando una particolare soluzione, da contattare per chiedere delucidazioni e supporto a livello locale (oltre, naturalmente, alle <a href="/community">svariate mailing lists attive</a>).
