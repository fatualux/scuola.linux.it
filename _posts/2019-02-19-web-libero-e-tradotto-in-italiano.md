---
categories:
- software
layout: post
title: Web Libero, e tradotto in Italiano
---
Grazie all'<a href="http://wiki.scuola.linux.it/doku.php?id=traduzioni">attività di alcuni volontari</a> procede l'attività di traduzione in italiano del sito e del software proposto da <a href="https://framasoft.org/">Framasoft</a>, associazione francese che da anni promuove l'utilizzo di applicazioni libere e aperte alternative a quelle dei colossi del Web (in primis: Google).

<!--more-->

Il principale intento è quello di sensibilizzare al tema della privacy e della protezione dei dati, e fornire soluzioni pratiche che possano essere effettivamente usate a casa, in classe o in ufficio, ma vanno ricordati i vantaggi di questo approccio nel settore didattico: il fatto di non dover condividere nessuna informazione sui propri studenti (minorenni, e pertanto fortemente e giustamente tutelati dalle norme vigenti), e la possibilità tecnica di ospitare tali applicazioni all'interno della propria rete scolastica aggirando le fin troppo frequenti limitazioni della connettività verso l'internet.

Ispirata da Framasoft è anche l'iniziativa <a href="http://www.ils.org/servizilinuxit">Servizi.Linux.it di Italian Linux Society</a>, che offre diversi servizi online e promuove gli strumenti che facilitano installazione e mantenimento di proprie istanze private da condividere sul web o usare nella propria rete locale.
