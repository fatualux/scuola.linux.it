---
categories:
- community
layout: post
title: Nasce la Rete PASW
---
In occasione dell'ultima <a href="http://porteapertesulweb.wikispaces.com/gasw2016">Giornata Aperta sul Web</a>, l'annuale assemblea di Porte Aperte sul Web, è stata annunciata la creazione dell'omonima <a href="http://www.porteapertesulweb.it/13-maggio-2016-nasce-rete-pasw/">Rete di Scuole</a>, con la quale la community si costituisce formalmente in una organizzazione strutturata e riconosciuta.

Laddove i canali di mutua assistenza tecnica ed amministrativa, ed i contenuti applicativi ed editoriali prodotti dalla community continueranno ad essere liberamente accessibili a chiunque, alla rete possono aderire tutte le scuole interessate ad avere un ruolo maggiormente attivo e che vogliono ufficializzare la propria adesione ai principi di condivisione propri di questa community.

Per l'adesione, o per maggiori informazioni sull'iniziativa, è possibile <a href="http://www.porteapertesulweb.it/il-sito/contatti/">contattare direttamente</a> i referenti di Porte Aperte sul Web.
