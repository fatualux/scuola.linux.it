---
categories:
- software
layout: post
title: Software Libero per la Scuola in Francia
---

A seguito della chiusura delle scuole in Francia a causa dell'epidemia del virus COVID-19, e della necessità di svolgere l'attività didattica online, il Ministero dell'Istruzione francese ha allestito <a href="http://apps.education.fr/">una raccolta di servizi e piattaforme</a> (tutti, ovviamente, open source) destinati ad essere usati dalle scuole del Paese e facilitare la transizione.

<!--more-->

Una scelta decisamente più proattiva e consapevole di quella del Ministero dell'Istruzione italiano, che si è limitato a pubblicare <a href="https://www.istruzione.it/coronavirus/didattica-a-distanza.html">una pagina web</a> che rimanda ai servizi online (chiusi e proprietari) offerti da diversi soggetti commerciali (con, in cima alla lista, Google e Microsoft), delegando a questi sia gli oneri tecnici che la raccolta dei dati di docenti e studenti e lasciando gli utenti completamente da soli nel barcamenarsi tra queste soluzioni.

Ancora una volta l'esperienza francese insegna che l'alternativa (sia tecnologica che metodologica) esiste e funziona, e tutto sta alla volontà e alla capacità politica di volerla adottare.
