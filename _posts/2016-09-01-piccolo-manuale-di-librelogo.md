---
categories:
- contenuti
layout: post
title: Piccolo Manuale di LibreLogo
---
<a href="https://groups.google.com/forum/#!topic/wii_libera_la_lavagna/wokYfC3nnYE">Annunciata qui</a> la disponibilità di un manuale in italiano per <a href="http://librelogo.org/en/">LibreLogo</a>, estensione di LibreOffice che permette di realizzare ed eseguire piccoli algoritmi a scopo didattico.

<!--more-->

Data la semplicità di installazione (basta avere LibreOffice, su Windows, Mac o Linux) e l'utilizzo del linguaggio <a href="https://it.wikipedia.org/wiki/Logo_(informatica)">Logo</a> (già conosciuto ed ancora usato da numerosi docenti), lo strumento si pone come una facile soluzione per l'insegnamento delle basi della programmazione ai più piccoli; una immediata alternativa al più popolare <a href="https://scratch.mit.edu/">Scratch</a>.

Si raccomanda di contattare l'autore, o scrivere sulla <a href="http://wiildos.it/">mailing list del gruppo Wii Libera la Lavagna</a>, per contribuire al miglioramento del manuale.
